#!/bin/bash

#cp *.html /mnt/lexa/public_html/pa055/.
#cp -R images /mnt/lexa/public_html/pa055/.
#cp -R site_libs /mnt/lexa/public_html/pa055/.
#cp -R lesson1_files /mnt/lexa/public_html/pa055/.


scp *.html lexa@helix.fi.muni.cz:/mnt/lexa/public_html/pa055/.
scp -r images lexa@helix.fi.muni.cz:/mnt/lexa/public_html/pa055/.
scp -r site_libs lexa@helix.fi.muni.cz:/mnt/lexa/public_html/pa055/.
scp -r lesson*_files lexa@helix.fi.muni.cz:/mnt/lexa/public_html/pa055/.
